## bugs

- [ ] notification tray icon is still broken lol

## minor bugs

- [ ] logs need to be HTML unescaped before saving

## personal wants

- [ ] inline profile viewer
- [ ] character select: avatar should link to profile

## ideas

- [ ] desktop: add option to disable custom titlebar? or maybe just remove it?
- [ ] desktop: add right click menu
- [ ] favorited eicons
  - when hovering over an eicon, show a star that adds it to a list of favorites
  - favorites accessible from bbc input (?)

## internal

- [x] un-workspaceify things
- [x] upgrade deps
- [x] add @vitejs/plugin-react
- [x] use tsup to compile desktop app
- [x] switch from jest to vitest
- [ ] look at dependency deprecations
