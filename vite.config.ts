/// <reference types="vite/client" />
/// <reference types="vitest/global" />
import react from "@vitejs/plugin-react"
import { defineConfig } from "vite"
import pkg from "./package.json"

const isDesktop = process.env.DESKTOP === "true"

// eslint-disable-next-line import/no-unused-modules
export default defineConfig({
  base: isDesktop ? "./" : "/",
  plugins: [react()],
  build: {
    sourcemap: true,
    outDir: isDesktop ? "dist/web-desktop" : "dist/web",
    // use targets that support top-level await
    target: ["edge89", "firefox89", "chrome89", "safari15", "node16"],
  },
  define: {
    APP_NAME: JSON.stringify(pkg.name),
    APP_VERSION: JSON.stringify(pkg.version),
    IS_DESKTOP: JSON.stringify(isDesktop),
  },
  test: {
    environment: "happy-dom",
    setupFiles: "src/setup-tests.ts",
    globalSetup: "src/global-setup.ts",
    global: true,
    threads: false,
  },
})

declare global {
  const APP_NAME: string
  const APP_VERSION: string
  const IS_DESKTOP: boolean
}
