import { app, ipcMain, shell } from "electron"
import JSZip from "jszip"
import * as fs from "node:fs/promises"
import { posix } from "node:path"

export async function addFilesystemIpcHandlers(
  rootPath = posix.join(app.getPath("userData"), "data"),
) {
  await fs.mkdir(rootPath, { recursive: true })

  // ensure that final path does not leave the root path
  const safeJoin = (...paths: string[]) => {
    const joined = posix.join(...paths)
    if (joined.startsWith(rootPath)) {
      return joined
    }
    throw new Error(`Path outside of root path: ${joined}`)
  }

  ipcMain.handle("fs-clear", async () => {
    await fs.rm(rootPath, { recursive: true })
    await fs.mkdir(rootPath, { recursive: true })
  })

  ipcMain.handle("fs-read", async (event, localPath: string) => {
    const path = safeJoin(rootPath, localPath)
    try {
      if (await isFile(path)) {
        return await fs.readFile(path, "utf8")
      }
    } catch (error) {
      console.warn(`Failed to read ${path}`, error)
    }
  })

  ipcMain.handle("fs-write", async (event, localPath: string, data: string) => {
    const path = safeJoin(rootPath, localPath)
    await fs.mkdir(posix.dirname(path), { recursive: true })
    await fs.writeFile(path, data)
  })

  ipcMain.handle("fs-delete", async (event, localPath: string) => {
    const path = safeJoin(rootPath, localPath)
    try {
      if (await exists(path)) {
        await fs.unlink(path)
      }
    } catch (error) {
      console.warn(`Failed to delete ${path}`, error)
    }
  })

  ipcMain.handle(
    "fs-append-line",
    async (event, localPath: string, line: string) => {
      const path = safeJoin(rootPath, localPath)
      try {
        await fs.mkdir(posix.dirname(path), { recursive: true })
        // eslint-disable-next-line unicorn/prefer-ternary
        if (await isFile(path)) {
          await fs.appendFile(path, `\n${line}`)
        } else {
          await fs.writeFile(path, line)
        }
      } catch (error) {
        console.warn(`Failed to append line to ${path}`, error)
      }
    },
  )

  ipcMain.handle("fs-export", async (_, localPath = "") => {
    const zip = new JSZip()

    const zipRootPath = safeJoin(rootPath, localPath)
    for await (const path of walkNestedPaths(zipRootPath)) {
      const data = await fs.readFile(path)
      const localPath = posix.relative(zipRootPath, path)

      // make sure we don't write backslashes on windows
      zip.file(localPath.replace(/\\/g, "/"), data)
    }

    return zip.generateAsync({ type: "arraybuffer" })
  })

  ipcMain.handle("fs-import", async (event, zipData: ArrayBuffer) => {
    const zip = await JSZip.loadAsync(zipData)

    for (const [path, file] of Object.entries(zip.files)) {
      const localPath = safeJoin(rootPath, path)
      if (file.dir) continue
      await fs.mkdir(posix.dirname(localPath), { recursive: true })
      await fs.writeFile(localPath, await file.async("nodebuffer"))
    }
  })

  ipcMain.on("open-folder", (event, path) => {
    void shell.openPath(safeJoin(rootPath, path))
  })

  ipcMain.handle("fs-read-directory-nested", async (event, localPath) => {
    const results = new Set<string>()

    for await (const nestedPath of walkNestedPaths(
      safeJoin(rootPath, localPath),
    )) {
      results.add(posix.relative(rootPath, nestedPath))
    }

    return results
  })
}

async function getStats(path: string) {
  try {
    return await fs.stat(path)
  } catch {
    return undefined
  }
}

async function isFile(path: string) {
  const stats = await getStats(path)
  return stats?.isFile()
}

async function exists(path: string) {
  const stats = await getStats(path)
  return stats?.isFile() || stats?.isDirectory()
}

async function* walkNestedPaths(path: string): AsyncGenerator<string> {
  const stats = await getStats(path)
  if (!stats?.isDirectory()) return

  const entries = await fs.readdir(path, { withFileTypes: true })
  for (const entry of entries) {
    const entryPath = posix.join(path, entry.name)
    if (entry.isFile()) {
      yield entryPath
    } else {
      yield* walkNestedPaths(entryPath)
    }
  }
}
