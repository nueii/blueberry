import type { BrowserWindow } from "electron"
import { app, ipcMain } from "electron"
import { readFile, writeFile } from "node:fs/promises"
import { join } from "node:path"
import { z } from "zod"

type WindowState = z.TypeOf<typeof windowStateSchema>
const windowStateSchema = z.object({
  rect: z.object({
    x: z.number(),
    y: z.number(),
    width: z.number(),
    height: z.number(),
  }),
  isMaximized: z.boolean(),
})

const defaultWindowState: WindowState = {
  isMaximized: true,
  rect: {
    x: 100,
    y: 100,
    width: 600,
    height: 800,
  },
}

const getWindowStatePath = () =>
  join(app.getPath("userData"), "window-state.json")

async function loadWindowState(): Promise<WindowState> {
  try {
    return windowStateSchema.parse(
      JSON.parse(await readFile(getWindowStatePath(), "utf8")),
    )
  } catch (error) {
    console.warn(error)
    return defaultWindowState
  }
}

export async function persistBrowserWindowState(win: BrowserWindow) {
  const state = await loadWindowState()
  if (state) {
    win.setBounds(state.rect)
    if (state.isMaximized) win.maximize()
    win.show()
  }

  function saveWindowState() {
    state.isMaximized = win.isMaximized()
    if (!win.isMaximized()) {
      state.rect = win.getBounds()
    }
    writeFile(getWindowStatePath(), JSON.stringify(state)).catch(console.warn)
  }

  win.on("moved", saveWindowState)
  win.on("resized", saveWindowState)
  win.on("maximize", saveWindowState)
  win.on("minimize", saveWindowState)
  win.on("restore", saveWindowState)
  win.on("unmaximize", saveWindowState)
}

export function addWindowStateIpcHandlers(win: BrowserWindow) {
  win.on("maximize", () => {
    win.webContents.send("maximized-changed", true)
  })
  win.on("restore", () => {
    win.webContents.send("maximized-changed", false)
  })
  win.on("unmaximize", () => {
    win.webContents.send("maximized-changed", false)
  })

  ipcMain.handle("get-maximized", () => win.isMaximized())
  ipcMain.handle("minimize", () => win.minimize())
  ipcMain.handle("maximize", () => win.maximize())
  ipcMain.handle("unmaximize", () => win.unmaximize())
  ipcMain.handle("close", () => win.hide())
}
