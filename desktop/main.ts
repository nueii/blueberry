import { app, dialog } from "electron"
import { posix } from "node:path"
import pkg from "../package.json"
import { isDev } from "./constants"
import { addFilesystemIpcHandlers } from "./filesystem-ipc"
import { createTray } from "./tray"
import { createWindow, loadWindowContent } from "./window"
import {
  addWindowStateIpcHandlers,
  persistBrowserWindowState,
} from "./window-state"

app.setName(isDev ? `${pkg.name}-dev` : pkg.name)

Promise.all([
  addFilesystemIpcHandlers(posix.join(app.getPath("userData"), "data")),
  app.whenReady().then(() => {
    const win = createWindow()

    return Promise.all([
      createTray(win),
      addWindowStateIpcHandlers(win),
      loadWindowContent(win),
      persistBrowserWindowState(win),
    ])
  }),
]).catch((error) => {
  dialog.showErrorBox(
    "An error occurred",
    error?.stack || error?.message || error,
  )
})
