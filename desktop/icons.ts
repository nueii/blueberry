/* eslint-disable unicorn/prefer-module */
import { nativeImage } from "electron"
import { join } from "node:path"

export const defaultIcon = nativeImage.createFromPath(
  join(__dirname, "../../assets/icon-16x.png"),
)

export const appIcon = nativeImage.createFromPath(
  join(__dirname, "../../assets/icon-spaced.png"),
)
