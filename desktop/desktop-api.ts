import { ipcRenderer } from "electron"
import { getPassword, setPassword } from "keytar"
import pkg from "../package.json"
import type { Emitter } from "./emitter"
import { createEmitter } from "./emitter"

export type Filesystem = {
  clear: () => Promise<void>
  file(path: string): FilesystemEntry
  exportFilesToZip(zipRootPath?: string): Promise<ArrayBuffer>
  importFilesFromZip(zip: ArrayBuffer): Promise<void>
  readDirectoryNested(path: string): Promise<Set<string>>
  onImport: Emitter<void>
}

export type FilesystemEntry = {
  read(): Promise<string | undefined>
  write(content: string): Promise<void>
  delete(): Promise<void>
  appendLine(line: string): Promise<void>
}

const filesystem: Filesystem = (() => {
  const onImport = createEmitter<void>()

  return {
    onImport,

    clear: async () => {
      await ipcRenderer.invoke("fs-clear")
    },

    file: (localPath) => {
      const entry: FilesystemEntry = {
        write: async (data) => {
          await ipcRenderer.invoke("fs-write", localPath, data)
        },
        read: async () => {
          return await ipcRenderer.invoke("fs-read", localPath)
        },
        delete: async () => {
          await ipcRenderer.invoke("fs-delete", localPath)
        },
        appendLine: async (line) => {
          await ipcRenderer.invoke("fs-append-line", localPath, line)
        },
      }

      return entry
    },

    exportFilesToZip: async (zipRootPath) => {
      return await ipcRenderer.invoke("fs-export", zipRootPath)
    },

    importFilesFromZip: async (zipData: ArrayBuffer) => {
      await ipcRenderer.invoke("fs-import", zipData)
      onImport.emit()
    },

    readDirectoryNested: async (path) => {
      return await ipcRenderer.invoke("fs-read-directory-nested", path)
    },
  }
})()

export const desktopApi = {
  filesystem,

  browserWindow: {
    isMaximized: (): Promise<boolean> => ipcRenderer.invoke("get-maximized"),

    minimize: () => ipcRenderer.invoke("minimize"),
    maximize: () => ipcRenderer.invoke("maximize"),
    unmaximize: () => ipcRenderer.invoke("unmaximize"),
    close: () => ipcRenderer.invoke("close"),

    focusWindow: () => ipcRenderer.send("focus-window"),
    flashWindow: () => ipcRenderer.send("flash-window"),

    handleMaximizeChanged: (callback: (maximized: boolean) => void) => {
      const handler = (event: Electron.IpcRendererEvent, maximized: boolean) =>
        callback(maximized)

      ipcRenderer.on("maximized-changed", handler)
      return () => {
        ipcRenderer.off("maximized-changed", handler)
      }
    },
  },

  setPassword: (account: string, password: string) =>
    setPassword(pkg.name, account, password),

  getPassword: (account: string) => getPassword(pkg.name, account),

  openLogsFolder: () => ipcRenderer.send("open-folder", "logs"),
}
