import type { BrowserWindow } from "electron"
import { Menu, Tray } from "electron"
import pkg from "../package.json"
import { createNativeImageFromUrl } from "./create-native-image-from-url"
import { defaultIcon } from "./icons"

let tray: Tray | undefined

export function createTray(win: BrowserWindow) {
  const createMenu = (title: string) =>
    Menu.buildFromTemplate([
      {
        label: title,
        enabled: false,
      },
      { type: "separator" },
      {
        label: "Show",
        click: () => win.show(),
      },
      {
        role: "hide",
        label: "Hide",
        click: () => win.hide(),
      },
      { type: "separator" },
      { role: "quit" },
    ])

  tray = new Tray(defaultIcon)

  tray.setContextMenu(createMenu(pkg.name))
  tray.setToolTip(pkg.name)

  tray.on("click", () => win.show())

  win.on("page-title-updated", (_, title) => {
    tray?.setToolTip(title)
    tray?.setContextMenu(createMenu(title))
  })

  win.webContents.on("page-favicon-updated", async (_, [url]) => {
    if (!url) return
    tray?.setImage(await createNativeImageFromUrl(url))
  })
}
