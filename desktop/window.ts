/* eslint-disable unicorn/prefer-module */
import { BrowserWindow, dialog, ipcMain, shell } from "electron"
import contextMenu from "electron-context-menu"
import { join } from "node:path"
import pkg from "../package.json"
import { getErrorStack } from "../src/common/getErrorStack.js"
import { isDev } from "./constants"
import { appIcon } from "./icons"

contextMenu({
  showSaveImageAs: true,
})

export function createWindow() {
  const win = new BrowserWindow({
    icon: appIcon,
    minWidth: 300,
    webPreferences: {
      preload: join(__dirname, "preload.cjs"),
      spellcheck: true,
    },
    show: false,
    title: pkg.name,
    frame: false,
  })

  win.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url).catch((error) => {
      dialog.showErrorBox("Failed to open link", getErrorStack(error))
    })
    return { action: "deny" }
  })

  ipcMain.on("focus-window", () => {
    win.show()
  })

  ipcMain.on("flash-window", () => {
    win.flashFrame(true)
  })

  return win
}

export async function loadWindowContent(win: BrowserWindow) {
  await (isDev
    ? win.loadURL(`http://localhost:3000`)
    : win.loadFile(join(__dirname, "../web-desktop/index.html")))
}
