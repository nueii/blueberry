import { nativeImage } from "electron"
import { fileURLToPath } from "node:url"

export async function createNativeImageFromUrl(url: string) {
  if (url.startsWith("file:")) {
    return nativeImage.createFromPath(fileURLToPath(url))
  }

  if (url.startsWith("http://localhost")) {
    const { got } = await import("got")
    return nativeImage.createFromBuffer(await got(url).buffer())
  }

  if (url.startsWith("data:")) {
    return nativeImage.createFromDataURL(url)
  }

  console.warn("Failed to create native image from url", url)
  return nativeImage.createEmpty()
}
