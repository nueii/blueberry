import ReactDOM from "react-dom"
import { addFaviconEventListeners } from "./app/favicon.js"
import Root from "./Root"
import { migrateLegacyStorage } from "./storage/migration"
import "./styles.css"

addFaviconEventListeners()
ReactDOM.createRoot(document.querySelector("#root")).render(<Root />)
migrateLegacyStorage().catch(console.error)
