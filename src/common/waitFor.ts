import { setTimeout } from "node:timers/promises"

/**
 * Re-runs a function until it doesn't throw,
 * or until a timeout is reached
 */
export async function waitFor<Result>(fn: () => Result | Promise<Result>) {
  const now = Date.now()
  // eslint-disable-next-line no-constant-condition
  while (true) {
    try {
      return await fn()
    } catch (error) {
      if (Date.now() - now > 4000) {
        throw error
      }
      await setTimeout(50)
    }
  }
}
