import { toError } from "./toError.js"

export function getErrorStack(error: unknown) {
  const { stack, message } = toError(error)
  return stack || message
}
