import JSZip from "jszip"
import { nanoid } from "nanoid"
import { existsSync, writeFileSync } from "node:fs"
import { mkdir } from "node:fs/promises"
import { dirname } from "node:path"
import { addFilesystemIpcHandlers } from "../../desktop/filesystem-ipc"
import { createDatabaseFilesystem } from "./databaseFilesystem"
import type { Filesystem } from "./filesystem"

function defineCommonTests(
  createFilesystem: () => Promise<Filesystem> | Filesystem,
) {
  let fs: Filesystem

  beforeAll(async () => {
    fs = await createFilesystem()
  })

  beforeEach(async () => {
    await fs.clear()
  })

  it("can read and write files", async () => {
    const content1 = `testing
testing2
testing3`

    const content2 = `testing
extreme testing
ahahaahahahaha
🍓`

    await Promise.all([
      fs.file("test1.txt").write(content1),
      fs.file("test2.txt").write(content2),
    ])

    expect(await fs.file("test1.txt").read()).toBe(content1)
    expect(await fs.file("test2.txt").read()).toBe(content2)
  })

  it("can append lines", async () => {
    const first = fs.file("first.txt")
    const second = fs.file("second.txt")

    await first.appendLine("line1")
    await second.appendLine("line3")
    await first.appendLine("line2")
    await second.appendLine("line4")

    expect(await first.read()).toBe("line1\nline2")
    expect(await second.read()).toBe("line3\nline4")
  })

  it("can delete files", async () => {
    const file = fs.file("test.txt")
    await file.write("test")
    expect(await file.read()).toBe("test")
    await file.delete()
    expect(await file.read()).toBeUndefined()
  })

  it("can import and export files", async () => {
    const files = [
      {
        name: "test.txt",
        content: "test",
      },
      {
        name: "nested/test.txt",
        content: "nested test",
      },
      {
        name: "nested/nested/nested/test.txt",
        content: "super nested test",
      },
    ]

    for (const file of files) {
      await fs.file(file.name).write(file.content)
    }

    const exported = await fs.exportFilesToZip()

    const zip = new JSZip()
    await zip.loadAsync(exported)
    expect(Object.values(zip.files).filter((file) => !file.dir).length).toBe(
      files.length,
    )

    for (const file of files) {
      const content = await zip.file(file.name)!.async("string")
      expect(content).toContain(file.content)
    }

    // write to an external file for manual inspection, if needed
    const zipFileName = `artifacts/test-${nanoid()}.zip`
    await mkdir(dirname(zipFileName), { recursive: true })
    writeFileSync(zipFileName, Buffer.from(exported))

    for (const file of files) {
      await fs.file(file.name).delete()
    }

    for (const file of files) {
      expect(await fs.file(file.name).read()).toBe(undefined)
    }

    await fs.importFilesFromZip(exported)
    for (const file of files) {
      expect(await fs.file(file.name).read()).toContain(file.content)
    }
  })

  it("can read nested directories", async () => {
    const files = [
      {
        name: "test.txt",
        content: "test",
      },
      {
        name: "nested/test.txt",
        content: "nested test",
      },
      {
        name: "nested/nested/nested/test.txt",
        content: "super nested test",
      },
    ]

    for (const file of files) {
      await fs.file(file.name).write(file.content)
    }

    const paths = await fs.readDirectoryNested("nested")

    expect(paths).toEqual(
      new Set(["nested/test.txt", "nested/nested/nested/test.txt"]),
    )
  })
}

describe("database filesystem", () => {
  defineCommonTests(() => createDatabaseFilesystem())
})

describe("native filesystem", () => {
  let handlersAdded = false
  const ensureHandlers = async () => {
    if (!handlersAdded) {
      handlersAdded = true
      await addFilesystemIpcHandlers("artifacts/native-fs")
    }
  }

  defineCommonTests(async () => {
    await ensureHandlers()
    return window.desktopApi?.filesystem!
  })

  describe("reading/writing outside sandbox directory", () => {
    beforeAll(() => {
      writeFileSync("artifacts/outside.txt", "file outside sandbox")
    })

    it("should not read directories", async () => {
      await ensureHandlers()
      const fs = window.desktopApi?.filesystem!

      await expect(() => fs.readDirectoryNested("..")).rejects.toThrow()
    })

    it("should not read files", async () => {
      await ensureHandlers()
      const fs = window.desktopApi?.filesystem!

      await expect(() => fs.file("../outside.txt").read()).rejects.toThrow()
    })

    it("should not write files", async () => {
      await ensureHandlers()
      const fs = window.desktopApi?.filesystem!

      await expect(() => fs.file("../new.txt").write("test")).rejects.toThrow()
      expect(existsSync("artifacts/new.txt")).toBe(false)
    })
  })
})
