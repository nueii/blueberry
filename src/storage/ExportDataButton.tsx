import { useChatContext } from "../chat/ChatContext"
import Button from "../dom/Button"
import { downloadObject } from "../dom/downloadObject"
import usePromiseState from "../state/usePromiseState"
import { solidButton } from "../ui/components"
import { defaultFilesystem } from "./filesystem"

export default function ExportDataButton() {
  const state = usePromiseState()
  const { notificationStore } = useChatContext()

  const downloadExportData = async () => {
    try {
      const zip = await defaultFilesystem.exportFilesToZip()
      downloadObject(`${APP_NAME}-data.zip`, zip)
    } catch (error) {
      // eslint-disable-next-line no-console
      console.warn(error)

      notificationStore.addToast({
        duration: 5000,
        details: {
          type: "error",
          message: "Failed to export, try again",
        },
      })
    }
  }

  return (
    <Button
      disabled={state.isLoading}
      className={solidButton}
      onClick={() => state.setPromise(downloadExportData())}
    >
      Export data
    </Button>
  )
}
