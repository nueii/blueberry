import type { JsonValue } from "type-fest"
import type { ZodSchema } from "zod"
import { defaultFilesystem } from "./filesystem"

export function jsonStorageHelpers<Value extends JsonValue | undefined>({
  path,
  schema,
  fallback,
}: {
  path: string
  schema: ZodSchema<Value>
  fallback: Value
}) {
  const parseValue = (value: string | undefined) => {
    try {
      if (!value) return fallback
      return schema.parse(JSON.parse(value))
    } catch (error) {
      console.warn(`Failed to parse ${path}`, error)
      return fallback
    }
  }

  const self = {
    get: async () => {
      return parseValue(await defaultFilesystem.file(path).read())
    },
    set: async (value: Readonly<Value>) => {
      await defaultFilesystem.file(path).write(JSON.stringify(value))
    },
    delete: async () => {
      await defaultFilesystem.file(path).delete()
    },
    update: async (updater: (value: Value) => Value) => {
      await self.set(updater(await self.get()))
    },
  }

  return self
}
