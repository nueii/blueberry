import { useChatContext } from "../chat/ChatContext"
import Button from "../dom/Button"
import { solidButton } from "../ui/components"
import { defaultFilesystem } from "./filesystem"

export default function ImportDataButton() {
  const { notificationStore } = useChatContext()
  return (
    <Button
      className={solidButton}
      onClick={() => {
        const input = document.createElement("input")
        input.type = "file"
        input.accept = ".zip"
        input.addEventListener("input", async () => {
          try {
            const file = input.files?.[0]
            if (!file) return

            await defaultFilesystem.importFilesFromZip(await file.arrayBuffer())

            notificationStore.addToast({
              onClick: () => {
                window.location.reload()
              },
              duration: 5000,
              details: {
                type: "info",
                message: "Data imported successfully!",
              },
            })
          } catch (error) {
            // eslint-disable-next-line no-console
            console.warn(error)

            notificationStore.addToast({
              duration: 5000,
              details: {
                type: "error",
                message: "Import failed, try again",
              },
            })
          }
        })
        input.click()
      }}
    >
      Import data
    </Button>
  )
}
