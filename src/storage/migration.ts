import { openDB } from "idb"
import { defaultFilesystem } from "./filesystem"

export async function migrateLegacyStorage() {
  const db = await openDB("keyval-store")

  const transaction = db.transaction("keyval", "readwrite")

  const nicknames = await transaction.store.get("characterNicknames")
  if (nicknames) {
    void defaultFilesystem
      .file("nicknames.json")
      .write(JSON.stringify(nicknames))

    await transaction.store.delete("characterNicknames")
  }

  for (const key of await transaction.store.getAllKeys()) {
    if (String(key).startsWith("channels:")) {
      const identity = String(key).split(":")[1]!
      const value = await transaction.store.get(key)
      const channelIds = value.channelsByIdentity[identity]
      if (!channelIds) continue

      void defaultFilesystem
        .file(`channels/${identity}.json`)
        .write(JSON.stringify(channelIds))

      await transaction.store.delete(key)
    }

    if (String(key).startsWith("privateChats:")) {
      const identity = String(key).split(":")[1]!
      const value = await transaction.store.get(key)
      const privateChatNames = value[identity]
      if (!privateChatNames) continue

      void defaultFilesystem
        .file(`private-chats/${identity}.json`)
        .write(JSON.stringify(privateChatNames))

      await transaction.store.delete(key)
    }
  }

  await transaction.done
}
