import { z } from "zod"
import type { MessageState } from "../message/MessageState"
import { messageStateSchema } from "../message/MessageState"
import { defaultFilesystem } from "./filesystem"
import { jsonStorageHelpers } from "./jsonStorage"

export function loggingStorage(roomName: string, identity: string) {
  const roomFolderPath = `logs/${roomName}`

  const logFilePath = `${roomFolderPath}/${identity}.log`
  const logFile = defaultFilesystem.file(logFilePath)

  const lastMessagesPath = `${roomFolderPath}/${identity}.last-messages.json`
  const lastMessageStorage = jsonStorageHelpers({
    path: lastMessagesPath,
    schema: z.array(messageStateSchema),
    fallback: [],
  })

  const self = {
    roomFolderPath,

    getLastMessages() {
      return lastMessageStorage.get()
    },

    addMessage(lastMessage: MessageState) {
      const timestamp = new Date(lastMessage.timestamp).toLocaleString()

      const logLine = (() => {
        if (!lastMessage.senderName) {
          return `[${timestamp}] ${lastMessage.text}`
        }
        if (lastMessage.type === "action") {
          return `[${timestamp}] ${lastMessage.senderName} ${lastMessage.text}`
        }
        return `[${timestamp}] ${lastMessage.senderName}: ${lastMessage.text}`
      })()

      logFile.appendLine(logLine).catch((error) => {
        console.warn(`Failed to log message to ${logFilePath}`, error)
      })
    },

    handleMessages(messages: readonly MessageState[]) {
      const lastMessage = messages[messages.length - 1]
      if (!lastMessage) return

      self.addMessage(lastMessage)

      lastMessageStorage.set(messages.slice(-200)).catch((error) => {
        console.warn(`Failed to save messages to ${lastMessagesPath}`, error)
      })
    },
  }

  return self
}
