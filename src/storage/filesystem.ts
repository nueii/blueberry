import type { Emitter } from "../state/emitter"
import { createDatabaseFilesystem } from "./databaseFilesystem"

export type Filesystem = {
  clear: () => Promise<void>
  file(path: string): FilesystemEntry
  exportFilesToZip(zipRootPath?: string): Promise<ArrayBuffer>
  importFilesFromZip(zip: ArrayBuffer): Promise<void>
  readDirectoryNested(path: string): Promise<Set<string>>
  onImport: Emitter<void>
}

export type FilesystemEntry = {
  read(): Promise<string | undefined>
  write(content: string): Promise<void>
  delete(): Promise<void>
  appendLine(line: string): Promise<void>
}

export const defaultFilesystem: Filesystem =
  window.desktopApi?.filesystem || createDatabaseFilesystem()
