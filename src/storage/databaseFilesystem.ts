import type { DBSchema } from "idb"
import { openDB } from "idb/with-async-ittr"
import { createEmitter } from "../state/emitter"
import type { Filesystem, FilesystemEntry } from "./filesystem"

type DBSchemaType<T extends DBSchema> = T

type FsDatabaseSchema = DBSchemaType<{
  lines: {
    value: {
      path: string
      text: string
    }
    key: number
    indexes: {
      path: string
    }
  }
}>

function openFilesystemDatabase() {
  return openDB<FsDatabaseSchema>("filesystem", 1, {
    upgrade(db) {
      // prettier-ignore
      db.createObjectStore("lines", { autoIncrement: true })
        .createIndex("path", "path")
    },
  })
}

/**
 * Creates a filesystem helper that uses an IndexedDB database to store files.
 *
 * Instead of a key/value approach,
 * the files are all stored as individual lines of text in a single object store.
 * This allows appending lines to files without loading the entire current file
 * into memory. For chat logs, which can become very large over time, this is
 * very useful.
 */
export function createDatabaseFilesystem(): Filesystem {
  const onImport = createEmitter<void>()

  const fs: Filesystem = {
    onImport,

    clear: async () => {
      const db = await openFilesystemDatabase()
      await db.clear("lines")
    },

    file: (path) => {
      const entry: FilesystemEntry = {
        async read(): Promise<string | undefined> {
          const db = await openFilesystemDatabase()
          const lines = await db.getAllFromIndex("lines", "path", path)
          return lines.length > 0
            ? lines.map((line) => line.text).join("\n")
            : undefined
        },

        async write(content: string) {
          const db = await openFilesystemDatabase()
          const transaction = db.transaction("lines", "readwrite")
          const store = transaction.store

          for await (const { primaryKey } of store
            .index("path")
            .iterate(IDBKeyRange.only(path))) {
            await store.delete(primaryKey)
          }

          for (const line of content.split(/\r?\n/)) {
            await store.add({ path, text: line })
          }

          await transaction.done
        },

        async delete() {
          const db = await openFilesystemDatabase()
          const transaction = db.transaction("lines", "readwrite")
          const store = transaction.store

          for await (const { primaryKey } of store
            .index("path")
            .iterate(IDBKeyRange.only(path))) {
            await store.delete(primaryKey)
          }

          await transaction.done
        },

        async appendLine(line: string) {
          const db = await openFilesystemDatabase()
          await db.add("lines", { path, text: line })
        },
      }
      return entry
    },

    async exportFilesToZip(zipRootPath = "") {
      const db = await openFilesystemDatabase()

      const linesForEachFile: Record<string, string[]> = {}
      for (const line of await db.getAll("lines")) {
        const path = normalizePath(line.path)
        if (path.startsWith(normalizePath(zipRootPath))) {
          const relativePath = normalizePath(path.slice(zipRootPath.length))
          const lines = (linesForEachFile[relativePath] ??= [])
          lines.push(line.text)
        }
      }

      const { default: JSZip } = await import("jszip")
      const zip = new JSZip()

      for (const [path, lines] of Object.entries(linesForEachFile)) {
        zip.file(path, lines.map((line) => line + "\n").join(""))
      }

      return zip.generateAsync({ type: "arraybuffer" })
    },

    async importFilesFromZip(zip: ArrayBuffer) {
      const db = await openFilesystemDatabase()

      const { loadAsync } = await import("jszip")
      const zipFile = await loadAsync(zip)

      const files = await Promise.all(
        Object.entries(zipFile.files).map(async ([path, file]) => ({
          path,
          text: await file.async("text"),
        })),
      )

      // Delete all existing files
      for (const { path } of files) {
        await fs.file(path).delete()
      }

      const transaction = db.transaction("lines", "readwrite")
      const store = transaction.store

      for (const { path, text } of files) {
        for (const line of text.split(/\r?\n/)) {
          await store.add({ path, text: line })
        }
      }

      await transaction.done

      onImport.emit()
    },

    async readDirectoryNested(root) {
      const db = await openFilesystemDatabase()

      const paths = new Set<string>()

      const transaction = db.transaction("lines", "readonly")
      const cursor = await transaction.store.openCursor()

      if (cursor) {
        for await (const current of cursor) {
          if (
            normalizePath(current.value.path).startsWith(normalizePath(root))
          ) {
            paths.add(current.value.path)
          }
        }
      }

      await transaction.done

      return paths
    },
  }

  return fs
}

function normalizePath(path: string) {
  return path
    .replace(/[/\\]+/g, "/") // Replace all slashes with a single slash
    .replace(/^[/\\]+/, "") // Remove leading slashes
    .replace(/[/\\]+$/, "") // Remove trailing slashes
}
