import createIPCMock from "electron-mock-ipc"
// @ts-expect-error
import FDBFactory from "fake-indexeddb/lib/FDBFactory"
import { rm } from "node:fs/promises"
import { desktopApi } from "../desktop/desktop-api"

window.desktopApi ??= desktopApi

vi.mock("electron", () => {
  const { ipcMain, ipcRenderer } = createIPCMock()
  return { ipcMain, ipcRenderer }
})

beforeEach(() => {
  globalThis.indexedDB = new FDBFactory()
})

beforeAll(async () => {
  await rm("artifacts", { recursive: true, force: true })
})

// https://stackoverflow.com/a/53449595/1332403
Object.defineProperty(window, "matchMedia", {
  writable: true,
  value: (query: string) => ({
    matches: false,
    media: query,
    onchange: undefined,
    addListener: () => {},
    removeListener: () => {},
    addEventListener: () => {},
    removeEventListener: () => {},
    dispatchEvent: () => {},
  }),
})
