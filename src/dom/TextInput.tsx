import type { ComponentProps } from "react"
import { autoRef } from "../react/autoRef"
import combineRefs from "../react/combineRefs"
import { useMountFocus } from "./useMountFocus"

type Props = ComponentProps<"input"> & {
  onChangeText?: (text: string) => void
  focusOnMount?: boolean
}

function TextInput({
  onChange,
  onChangeText,
  focusOnMount = false,
  ref,
  ...props
}: Props) {
  const mountFocusRef = useMountFocus(focusOnMount)
  return (
    <input
      {...props}
      onChange={(event) => {
        onChange?.(event)
        onChangeText?.(event.target.value)
      }}
      ref={combineRefs(ref, mountFocusRef)}
    />
  )
}

export default autoRef<Props, HTMLInputElement>(TextInput)
