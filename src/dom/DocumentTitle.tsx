import type { ReactNode } from "react"
import { createContext, useContext, useEffect, useMemo, useState } from "react"

type ContextType = {
  setTitle(title: string): void
}

const Context = createContext<ContextType | undefined>()

export default function DocumentTitle({
  title,
  children,
}: {
  title: string
  children: ReactNode
}) {
  const parentContext = useContext(Context)
  const [childTitle, setChildTitle] = useState<string>()
  const fullTitle = [childTitle, title].filter(Boolean).join(" | ")

  useEffect(() => {
    if (!parentContext) {
      document.title = fullTitle
      return
    }

    parentContext.setTitle(title)
    return () => {
      parentContext.setTitle("")
    }
  }, [fullTitle, parentContext, title])

  const context = useMemo(() => ({ setTitle: setChildTitle }), [])

  return <Context.Provider value={context}>{children}</Context.Provider>
}
