import { useEffect, useRef } from "react"
import { raise } from "../common/raise"

export function useMountFocus(enabled = true) {
  const ref = useRef<HTMLElement | null>()

  useEffect(() => {
    if (!enabled) return
    if (!ref.current) {
      raise("[useMountFocus] ref not assigned")
    }
    ref.current.focus()
  }, [enabled])

  // using a callback works more elegantly with typescript
  return (element: HTMLElement | null) => {
    ref.current = element
  }
}
