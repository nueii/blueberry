export function downloadObject(
  filename: string,
  object: Blob | MediaSource | ArrayBuffer,
) {
  if (object instanceof ArrayBuffer) {
    object = new Blob([object])
  }

  const a = document.createElement("a")
  a.href = URL.createObjectURL(object)
  a.download = filename
  a.click()
}
