import defaultIcon from "../../assets/icon-16x.png"
import notificationIcon from "../../assets/icon-notification-16x.png"
import { raise } from "../common/raise"

const notificationIconLink =
  document.querySelector<HTMLLinkElement>("link#favicon") ??
  raise("Could not find favicon element")

let hasFocus = document.hasFocus()

export function addFaviconEventListeners() {
  document.addEventListener("focus", () => {
    hasFocus = true
    notificationIconLink.href = defaultIcon
  })

  document.addEventListener("blur", () => {
    hasFocus = false
  })
}

export function applyNotificationFavicon() {
  if (hasFocus) return
  notificationIconLink.href = notificationIcon
}
