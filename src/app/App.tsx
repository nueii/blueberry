import { lazy, Suspense, useState } from "react"
import type { LoginResponse } from "../flist/api"
import { createFListApi } from "../flist/api"
import IslandLayout from "../ui/IslandLayout"
import LoadingOverlay from "../ui/LoadingOverlay"
import Modal from "../ui/Modal"
import AppErrorBoundary from "./AppErrorBoundary"
import AppInfo from "./AppInfo"
import CharacterSelect from "./CharacterSelect"
import Login from "./Login"

const ChatRoot = lazy(() => import("../chat/ChatRoot"))

type AppView =
  | { name: "login" }
  | { name: "characterSelect"; user: LoginResponse }
  | { name: "chat"; user: LoginResponse; identity: string }

export default function App() {
  const [view, setView] = useState<AppView>({ name: "login" })
  const [api] = useState(createFListApi)

  if (view.name === "login") {
    return (
      <Suspense fallback={<LoadingOverlay text="Loading..." />}>
        <IslandLayout title="Login" isVisible header={<AppInfoModalButton />}>
          <Login
            onSubmit={async (credentials) => {
              const user = await api.login(credentials)
              setView({ name: "characterSelect", user })
            }}
          />
        </IslandLayout>
      </Suspense>
    )
  }

  if (view.name === "characterSelect") {
    return (
      <IslandLayout title="Select a Character" isVisible>
        <CharacterSelect
          user={view.user}
          onSubmit={(identity) => {
            setView({ name: "chat", user: view.user, identity })
          }}
          onBack={() => {
            setView({ name: "login" })
          }}
        />
      </IslandLayout>
    )
  }

  if (view.name === "chat") {
    return (
      <AppErrorBoundary>
        <Suspense fallback={<LoadingOverlay text="Loading..." />}>
          <ChatRoot
            user={view.user}
            identity={view.identity}
            api={api}
            onShowLogin={() => {
              setView({ name: "login" })
            }}
            onShowCharacterSelect={() => {
              setView({ name: "characterSelect", user: view.user })
            }}
          />
        </Suspense>
      </AppErrorBoundary>
    )
  }

  return <></>
}

function AppInfoModalButton() {
  return (
    <Modal
      title={`About ${APP_NAME}`}
      renderTrigger={(t) => (
        <button className="text-center" {...t}>
          <AppInfo.Heading />
          <p className="opacity-75">click for more info</p>
        </button>
      )}
      renderContent={() => (
        <div className="p-4">
          <AppInfo />
        </div>
      )}
    />
  )
}
