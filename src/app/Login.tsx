import { useEffect, useState } from "react"
import { clear, suspend } from "suspend-react"
import Button from "../dom/Button"
import TextInput from "../dom/TextInput"
import type { LoginCredentials } from "../flist/types"
import { preventDefault } from "../react/preventDefault"
import usePromiseState from "../state/usePromiseState"
import { defaultFilesystem } from "../storage/filesystem"
import Checkbox from "../ui/Checkbox"
import { input, solidButton } from "../ui/components"
import FormField from "../ui/FormField"

const usernameFile = defaultFilesystem.file("login/username.txt")
const rememberFile = defaultFilesystem.file("login/remember.txt")

export default function Login(props: {
  onSubmit: (credentials: LoginCredentials) => Promise<unknown> | void
}) {
  const initialState = suspend(async () => {
    const remember = await rememberFile.read()
    if (remember !== "true") return

    const username = await usernameFile.read()
    if (!username) return

    const password = (await window.desktopApi?.getPassword(username)) ?? ""
    return { username, password }
  }, ["initialLoginState"])

  const [account, setAccount] = useState(initialState?.username ?? "")
  const [password, setPassword] = useState(initialState?.password ?? "")
  const [remember, setRemember] = useState(!!initialState)

  useEffect(() => {
    void rememberFile.write(remember ? "true" : "false")
  }, [remember])

  useEffect(() => {
    if (remember) {
      void usernameFile.write(account)
    } else {
      void usernameFile.delete()
    }
  }, [account, remember])

  useEffect(() => () => clear(["initialLoginState"]), [])

  const authenticateState = usePromiseState()

  const submit = () => {
    if (authenticateState.isLoading) return
    authenticateState.setPromise(props.onSubmit({ account, password }))
    void window.desktopApi?.setPassword(account, password)
  }

  const canSubmit =
    account !== "" && password !== "" && !authenticateState.isLoading

  const isFormDisabled = authenticateState.isLoading

  return (
    <form
      className="flex flex-col items-start p-4 space-y-4"
      onSubmit={preventDefault(submit)}
    >
      <FormField labelText="Username">
        <TextInput
          className={input}
          type="text"
          placeholder="awesome username"
          autoComplete="username"
          value={account}
          onChangeText={setAccount}
          disabled={isFormDisabled}
          focusOnMount
        />
      </FormField>

      <FormField labelText="Password">
        <TextInput
          className={input}
          type="password"
          placeholder="••••••••"
          autoComplete="current-password"
          value={password}
          onChangeText={setPassword}
          disabled={isFormDisabled}
        />
      </FormField>

      <Checkbox
        label="Remember me"
        checked={remember}
        onCheckedChange={setRemember}
        disabled={isFormDisabled}
      />

      <Button className={solidButton} type="submit" disabled={!canSubmit}>
        Log in
      </Button>

      {authenticateState.error && (
        <p className="max-w-xs">{authenticateState.error.message}</p>
      )}
    </form>
  )
}
