import { z } from "zod"
import { uniqueId } from "../common/uniqueId"

const actionRegex = /^\s*\/me\s*/
const warningRegex = /^\s*\/warn\s*/

export type MessageType = z.TypeOf<typeof messageTypeSchema>
export const messageTypeSchema = z.union([
  z.literal("normal"),
  z.literal("action"),
  z.literal("lfrp"),
  z.literal("warning"),
  z.literal("system"),
])

export type MessageState = z.TypeOf<typeof messageStateSchema>
export const messageStateSchema = z.object({
  key: z.string(),
  text: z.string(),
  type: messageTypeSchema,
  timestamp: z.number(),
  senderName: z.string().optional(),
})

export function createMessageState(args: {
  text: string
  type: MessageType
  senderName?: string
  timestamp?: number
  key?: string
}): MessageState {
  return {
    ...args,
    key: args.key || uniqueId(),
    timestamp: args.timestamp || Date.now(),
  }
}

export function createChannelMessage(senderName: string, text: string) {
  const type = ((): MessageType => {
    if (actionRegex.test(text)) return "action"
    if (warningRegex.test(text)) return "warning"
    return "normal"
  })()

  return createMessageState({
    senderName,
    text: text.replace(actionRegex, "").replace(warningRegex, ""),
    type,
  })
}

export function createPrivateMessage(senderName: string, text: string) {
  const type: MessageType = actionRegex.test(text) ? "action" : "normal"

  return createMessageState({
    senderName,
    text: text.replace(actionRegex, ""),
    type,
  })
}

export function createAdMessage(senderName: string, text: string) {
  return createMessageState({ senderName, text, type: "lfrp" })
}

export function createSystemMessage(text: string) {
  return createMessageState({ text, type: "system" })
}
