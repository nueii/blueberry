import type { ReactNode } from "react"
import { Fragment, useEffect, useState } from "react"
import appIcon from "../../assets/icon.svg"
import Icon from "./Icon"
import {
  windowClose,
  windowMaximize,
  windowMinimize,
  windowRestore,
} from "./icons"

function WindowFrame({ children }: { children: ReactNode }) {
  const [maximized, setMaximized] = useState(false)

  useEffect(() => {
    void window.desktopApi?.browserWindow.isMaximized().then(setMaximized)
  }, [])

  useEffect(() => {
    return window.desktopApi?.browserWindow.handleMaximizeChanged(setMaximized)
  }, [])

  return (
    <div className="fixed inset-0 flex flex-col">
      <div
        className="flex flex-wrap items-center"
        style={{ WebkitAppRegion: "drag" }}
      >
        <img src={appIcon} alt="App icon" className="w-5 h-5 mx-2 opacity-75" />
        <h1 className="py-1 opacity-50 font-condensed whitespace-nowrap">
          {APP_NAME}
        </h1>
        <div className="flex self-stretch ml-auto ">
          <WindowTitleButton
            title="Minimize"
            onClick={window.desktopApi?.browserWindow.minimize}
          >
            <Icon which={windowMinimize} size={5} />
          </WindowTitleButton>

          {maximized ? (
            <WindowTitleButton
              title="Restore"
              onClick={window.desktopApi?.browserWindow.unmaximize}
            >
              <Icon which={windowRestore} size={5} />
            </WindowTitleButton>
          ) : (
            <WindowTitleButton
              title="Maximize"
              onClick={window.desktopApi?.browserWindow.maximize}
            >
              <Icon which={windowMaximize} size={5} />
            </WindowTitleButton>
          )}

          <WindowTitleButton
            title="Close"
            onClick={window.desktopApi?.browserWindow.close}
          >
            <Icon which={windowClose} size={5} />
          </WindowTitleButton>
        </div>
      </div>
      <div className="flex-1 min-h-0">{children}</div>
    </div>
  )
}

function WindowTitleButton({
  title,
  children,
  onClick,
}: {
  title: string
  children: ReactNode
  onClick?: () => void
}) {
  return (
    <button
      title={title}
      className="px-2 py-1 transition opacity-50 hover:opacity-100"
      style={{ WebkitAppRegion: "no-drag" }}
      onClick={onClick}
    >
      {children}
    </button>
  )
}

export default IS_DESKTOP ? WindowFrame : Fragment

declare module "react" {
  // eslint-disable-next-line @typescript-eslint/consistent-type-definitions
  interface CSSProperties {
    WebkitAppRegion?: "drag" | "no-drag" | undefined
  }
}
