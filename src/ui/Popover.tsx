import { Portal, Transition } from "@headlessui/react"
import { clamp } from "lodash-es"
import type { CSSProperties } from "react"
import { useDeferredValue, useState } from "react"
import { FocusOn } from "react-focus-on"
import { useElementSize } from "../dom/useElementSize"
import { useWindowSize } from "../dom/useWindowSize"
import FadeRiseTransition from "./FadeRiseTransition"

type TriggerProps = {
  onClick: (event: React.MouseEvent) => void
}

const gutter = 20

export default function Popover({
  trigger,
  content,
}: {
  trigger: (triggerProps: TriggerProps) => React.ReactNode
  content: () => React.ReactNode
}) {
  const [popover, popoverRef] = useState<HTMLElement | null>()
  const [position, setPosition] = useState({ x: 0, y: 0 })

  const [open, setOpen] = useState(false)
  const deferredOpen = useDeferredValue(open)

  const elementSize = useElementSize(popover)
  const windowSize = useWindowSize()

  const x = clamp(
    position.x,
    gutter,
    windowSize.width - elementSize.width - gutter,
  )

  const y = clamp(
    position.y,
    gutter,
    windowSize.height - elementSize.height - gutter,
  )

  const style: CSSProperties = {
    transform: `translate(${x}px, ${y}px)`,
    maxHeight: `${windowSize.height - gutter * 2}px`,
  }

  return (
    <>
      {trigger({
        onClick: (event) => {
          setOpen(!open)
          setPosition({ x: event.clientX, y: event.clientY })
        },
      })}
      <Transition.Root className="contents" show={open}>
        <Portal>
          <FadeRiseTransition className="fixed top-0 left-0" child>
            <FocusOn
              ref={popoverRef}
              className="overflow-y-auto shadow"
              style={style}
              onClickOutside={() => setOpen(false)}
              // the focus trap takes a bit of time to apply when there are more elements on the screen,
              // and deferring the enabled state allows the transition to run while that's happening
              enabled={deferredOpen}
            >
              {content()}
            </FocusOn>
          </FadeRiseTransition>
        </Portal>
      </Transition.Root>
    </>
  )
}
