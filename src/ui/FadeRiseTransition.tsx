import clsx from "clsx"
import { createTransitionComponent } from "./createTransitionComponent"

const FadeRiseTransition = createTransitionComponent({
  enterFrom: clsx`translate-y-4 opacity-0`,
  enterTo: clsx`translate-y-0 opacity-100`,
})

export default FadeRiseTransition
