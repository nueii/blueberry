import * as React from "react"
import { formLabelClass } from "./components"

type Props = {
  labelText: string
  children: React.ReactNode
}

function FormField({ labelText, children }: Props) {
  return (
    <label className={`block w-full`}>
      <div className={formLabelClass}>{labelText}</div>
      {children}
    </label>
  )
}

export default FormField
