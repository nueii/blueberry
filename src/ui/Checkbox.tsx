import clsx from "clsx"
import type { ReactNode } from "react"
import { activePressClass } from "./helpers"
import Icon from "./Icon"
import { checkFilled, checkOutline } from "./icons"

export default function Checkbox({
  label,
  disabled,
  checked,
  onCheckedChange,
}: {
  label: ReactNode
  disabled?: boolean
  checked: boolean
  onCheckedChange: (checked: boolean) => void
}) {
  return (
    <label
      className={clsx(
        "cursor-pointer transition duration-250 inline-block",
        activePressClass,
        disabled && "opacity-50",
      )}
    >
      <input
        type="checkbox"
        className="sr-only peer"
        aria-disabled={disabled}
        checked={checked}
        onChange={(event) => {
          if (!disabled) {
            onCheckedChange(event.target.checked)
          }
        }}
      />
      <div className="flex items-center gap-1 pr-1 peer-focus-visible:ring-2">
        <Icon which={checked ? checkFilled : checkOutline} />
        <span className="translate-y-[1px] select-none">{label}</span>
      </div>
    </label>
  )
}
