import clsx from "clsx"

export const ellipsize = "overflow-hidden whitespace-nowrap overflow-ellipsis"

export const activePressClass = clsx`active:transform active:translate-y-0.5 active:transition-none`
