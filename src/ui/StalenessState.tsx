import clsx from "clsx"
import type { ReactNode } from "react"
import LoadingIcon from "./LoadingIcon"

export default function StalenessState({
  isStale,
  children,
}: {
  isStale: boolean
  children: ReactNode
}) {
  return (
    <div
      className={clsx(
        "h-full transition-opacity relative",
        isStale ? "opacity-50 transition-delay-300" : "opacity-100",
      )}
    >
      {children}
      <div
        className={clsx(
          "absolute inset-0 grid pointer-events-none place-content-center transition-opacity",
          isStale ? "opacity-100 transition-delay-300" : "opacity-0",
        )}
      >
        <LoadingIcon />
      </div>
    </div>
  )
}
