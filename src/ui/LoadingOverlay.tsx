import LoadingIcon from "./LoadingIcon"

export default function LoadingOverlay({ text }: { text: string }) {
  return (
    <div
      className={`fixed inset-0 flex flex-col items-center justify-center text-center bg-black bg-opacity-50`}
    >
      <LoadingIcon />
      <div className="flex flex-col items-center gap-4">
        <p className={`text-2xl font-condensed`}>{text}</p>
      </div>
    </div>
  )
}
