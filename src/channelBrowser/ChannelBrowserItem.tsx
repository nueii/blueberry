import clsx from "clsx"
import { memo } from "react"
import { useChatContext } from "../chat/ChatContext"
import { useStoreValue } from "../state/store"
import Icon from "../ui/Icon"
import { earth, lock } from "../ui/icons"
import type { ChannelBrowserChannel } from "./types"

type Props = {
  info: ChannelBrowserChannel
  icon?: React.ReactNode
}

function ChannelBrowserItem({ info, icon }: Props) {
  const context = useChatContext()

  const isJoined = useStoreValue(
    context.channelStore.channels.select(
      (channels) => channels[info.id]?.joinState === "present",
    ),
  )

  const handleClick = () => {
    if (isJoined) {
      context.channelStore.leave(info.id)
    } else {
      context.channelStore.join(info.id, info.title)
    }
  }

  return (
    <button
      className={clsx(
        `flex flex-row items-center px-2 py-2 space-x-2 transition-all w-full`,
        isJoined ? `opacity-100 bg-midnight-0` : `opacity-50 hover:opacity-75`,
      )}
      onClick={handleClick}
    >
      {icon ?? <Icon which={info.type === "public" ? earth : lock} />}
      <div
        className="flex-1 overflow-hidden whitespace-nowrap overflow-ellipsis"
        dangerouslySetInnerHTML={{ __html: info.title }}
      />
      <div className={`w-12 text-right`}>{info.userCount}</div>
    </button>
  )
}

export default memo(ChannelBrowserItem)
