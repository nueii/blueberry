import { delay } from "../common/delay"
import type { Dict } from "../common/types"
import type { ChatSocket } from "../socket/ChatSocket"
import type { ServerCommand } from "../socket/helpers"
import { matchCommand } from "../socket/helpers"
import type { Store } from "../state/store"
import { createStore } from "../state/store"
import type { ChannelBrowserChannel } from "./types"

export type ChannelBrowserStore = ReturnType<typeof createChannelBrowserStore>

function createChannelInfoDict(
  type: ChannelBrowserChannel["type"],
  entries: ReadonlyArray<{
    name: string
    title?: string
    characters: number
  }>,
): Dict<ChannelBrowserChannel> {
  return Object.fromEntries(
    entries.map((it) => [
      it.name,
      {
        id: it.name,
        title: it.title ?? it.name,
        userCount: it.characters,
        type,
      },
    ]),
  )
}

export function createChannelBrowserStore(socket: ChatSocket) {
  const channels = createStore<{
    public: Dict<ChannelBrowserChannel>
    private: Dict<ChannelBrowserChannel>
  }>({
    public: {},
    private: {},
  })

  const isRefreshing = createStore(false)

  const store = {
    channels,
    isRefreshing,

    async refresh() {
      if (isRefreshing.value) return

      isRefreshing.set(true)

      socket.send({ type: "CHA" })
      socket.send({ type: "ORS" })

      // the server has a 7 second timeout on refreshes
      isRefreshing.set(true)
      await delay(7000)
      isRefreshing.set(false)
    },

    selectChannelInfo(channelId: string): Store<ChannelBrowserChannel> {
      return channels.select(
        (channels) =>
          channels.public[channelId] ??
          channels.private[channelId] ?? {
            id: channelId,
            title: channelId,
            type: "public",
            userCount: 0,
          },
      )
    },

    selectUserCount(channelId: string) {
      return channels.select((channels) => {
        const info = channels.public[channelId] ?? channels.private[channelId]
        return info?.userCount ?? 0
      })
    },

    selectIsPublic(channelId: string) {
      return channels.select((channels) => !!channels.public[channelId])
    },

    selectChannelLink(channelId: string) {
      return channels.select((channels) => {
        const channel = channels.public[channelId]
        if (!channel) {
          return `[channel]${channelId}[/channel]`
        }
        return `[session=${channel.title}]${channelId}[/session]`
      })
    },

    handleCommand: (command: ServerCommand) => {
      matchCommand(command, {
        IDN() {
          // perform a refresh so user counts on channel links are up to date
          void store.refresh()
        },

        CHA(params) {
          channels.mergeSet({
            public: createChannelInfoDict("public", params.channels),
          })
        },

        ORS(params) {
          channels.mergeSet({
            private: createChannelInfoDict("private", params.channels),
          })
        },
      })
    },
  }

  return store
}
