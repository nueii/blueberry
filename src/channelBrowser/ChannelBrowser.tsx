import clsx from "clsx"
import { sortBy } from "lodash-es"
import { matchSorter } from "match-sorter"
import { Suspense, useEffect, useMemo, useState } from "react"
import { getAllStoredJoinedChannels } from "../channel/ChannelStore"
import { useChatContext } from "../chat/ChatContext"
import { isTruthy } from "../common/isTruthy"
import Button from "../dom/Button"
import TextInput from "../dom/TextInput"
import { useTemporarySuspenseValue } from "../react/useTemporarySuspenseValue"
import { useStoreValue } from "../state/store"
import { input, solidButton } from "../ui/components"
import Icon from "../ui/Icon"
import * as icons from "../ui/icons"
import LoadingIcon from "../ui/LoadingIcon"
import VirtualizedList from "../ui/VirtualizedList"
import ChannelBrowserItem from "./ChannelBrowserItem"
import type { ChannelBrowserChannel } from "./types"

type SortMode = "title" | "userCount"

type ListItem = {
  key: string
  kind: "frequent" | "default"
  channel: ChannelBrowserChannel
}

export default function ChannelBrowser() {
  return (
    <div className={`flex flex-col w-full h-full`}>
      <Suspense fallback={<LoadingIcon />}>
        <ChannelBrowserInternal />
      </Suspense>
    </div>
  )
}

function ChannelBrowserInternal() {
  const context = useChatContext()
  const [query, setQuery] = useState("")

  const [sortMode, setSortMode] = useState<SortMode>("title")

  useEffect(() => {
    void context.channelBrowserStore.refresh()
  }, [context.channelBrowserStore])

  const cycleSortMode = () =>
    setSortMode((mode) => (mode === "title" ? "userCount" : "title"))

  const publicChannels = useStoreValue(
    context.channelBrowserStore.channels.select((it) => it.public),
    Object.is,
  )

  const privateChannels = useStoreValue(
    context.channelBrowserStore.channels.select((it) => it.private),
    Object.is,
  )

  const frequentChannelIds = useTemporarySuspenseValue(
    getAllStoredJoinedChannels,
    ["frequentlyJoinedChannels"],
  )

  const listItems = useMemo(() => {
    const frequentChannels: ListItem[] = frequentChannelIds
      .map((id) => publicChannels[id] ?? privateChannels[id])
      .filter(isTruthy)
      .map((channel) => ({
        channel,
        key: `frequent-${channel.id}`,
        kind: "frequent" as const,
      }))
      .sort((a, b) =>
        a.channel.title
          .toLocaleLowerCase()
          .localeCompare(b.channel.title.toLocaleLowerCase()),
      )

    const allChannels: ListItem[] = [
      ...processChannels(Object.values(publicChannels), query, sortMode),
      ...processChannels(Object.values(privateChannels), query, sortMode),
    ].map((channel) => ({
      channel,
      key: `default-${channel.id}`,
      kind: "default",
    }))

    return [...frequentChannels, ...allChannels]
  }, [publicChannels, privateChannels, query, sortMode, frequentChannelIds])

  const isRefreshing = useStoreValue(context.channelBrowserStore.isRefreshing)

  return (
    <div className={`flex flex-col w-full h-full`}>
      <section
        className={`bg-midnight-2`}
        style={{ height: "calc(100vh - 10rem)" }}
      >
        <VirtualizedList
          items={listItems}
          getItemKey={(item) => item.key}
          itemSize={40}
          renderItem={({ item, style }) => (
            <div style={style}>
              <ChannelBrowserItem
                info={item.channel}
                icon={
                  item.kind === "default" ? undefined : (
                    <Icon which={icons.star} />
                  )
                }
              />
            </div>
          )}
        />
      </section>

      <section className="flex flex-row gap-2 p-2 bg-midnight-0">
        <TextInput
          type="text"
          aria-label="Search"
          placeholder="Search..."
          className={clsx(input, `flex-1`)}
          value={query}
          onChangeText={setQuery}
          focusOnMount
        />

        <Button
          title="Change sort mode"
          className={solidButton}
          onClick={cycleSortMode}
        >
          {sortMode === "title" ? (
            <Icon which={icons.sortAlphabetical} />
          ) : (
            <Icon which={icons.sortNumeric} />
          )}
        </Button>

        <Button
          title="Refresh"
          className={solidButton}
          onClick={context.channelBrowserStore.refresh}
          disabled={isRefreshing}
        >
          <Icon which={icons.refresh} />
        </Button>
      </section>
    </div>
  )
}

function processChannels(
  channels: ChannelBrowserChannel[],
  query: string,
  sortMode: SortMode,
) {
  const sorted =
    sortMode === "title"
      ? sortBy(channels, (it) => it.title.toLowerCase())
      : sortBy(channels, "userCount").reverse()

  if (!query.trim()) {
    return sorted
  }

  return matchSorter(sorted, query, {
    keys: ["id", "title"],
  })
}
