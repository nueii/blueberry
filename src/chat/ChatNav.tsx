import AppInfo from "../app/AppInfo"
import ChannelBrowser from "../channelBrowser/ChannelBrowser"
import CharacterSummary from "../character/CharacterSummary"
import OnlineUsers from "../character/OnlineUsers"
import Button from "../dom/Button"
import { downloadObject } from "../dom/downloadObject"
import NotificationListLink from "../notifications/NotificationListLink"
import { useRoute } from "../router"
import SettingsScreen from "../settings/SettingsScreen"
import { defaultFilesystem } from "../storage/filesystem"
import Icon from "../ui/Icon"
import * as icons from "../ui/icons"
import LogsIcon from "../ui/LogsIcon"
import Modal from "../ui/Modal"
import { useChatContext } from "./ChatContext"
import ChatNavAction from "./ChatNavAction"
import ChatNavActionButton from "./ChatNavActionButton"
import { clearStoredStatus } from "./createStatusPersistenceHandler"
import RoomTabList from "./RoomTabList"
import StatusUpdateForm from "./StatusUpdateForm"

export default function ChatNav() {
  const context = useChatContext()
  const route = useRoute()

  return (
    <nav className="grid h-full grid-cols-[auto,15rem] bg-midnight-2">
      <div className="flex flex-col overflow-y-auto">
        <Modal
          title="Channel Browser"
          renderContent={() => <ChannelBrowser />}
          renderTrigger={(t) => (
            <ChatNavActionButton
              icon={<Icon which={icons.list} />}
              name="Browse channels"
              {...t}
            />
          )}
        />

        <Modal
          title="Online Users"
          renderTrigger={(t) => (
            <ChatNavActionButton
              icon={<Icon which={icons.users} />}
              name="Online Users"
              {...t}
            />
          )}
          renderContent={() => (
            <div className="h-[calc(100vh-8rem)]">
              <OnlineUsers />
            </div>
          )}
        />

        <Modal
          title="Status update"
          renderContent={({ close }) => <StatusUpdateForm onSuccess={close} />}
          renderTrigger={(t) => (
            <ChatNavActionButton
              icon={<Icon which={icons.updateStatus} />}
              name="Update your status"
              {...t}
            />
          )}
        />

        <NotificationListLink />

        <ChatNavActionButton
          icon={<LogsIcon />}
          name="View logs"
          onClick={async () => {
            if (window.desktopApi) {
              window.desktopApi.openLogsFolder()
            } else {
              downloadObject(
                `${APP_NAME}-logs.zip`,
                await defaultFilesystem.exportFilesToZip("logs"),
              )
            }
          }}
        />

        <Modal
          title="Settings"
          renderTrigger={(t) => (
            <ChatNavActionButton
              icon={<Icon which={icons.settings} />}
              name="Settings"
              {...t}
            />
          )}
          renderContent={() => (
            <div className="p-4">
              <SettingsScreen />
            </div>
          )}
        />

        <Modal
          title={`About ${APP_NAME}`}
          renderTrigger={(t) => (
            <ChatNavActionButton
              icon={<Icon which={icons.about} />}
              name={`About ${APP_NAME}`}
              {...t}
            />
          )}
          renderContent={() => (
            <div className="p-4">
              <AppInfo />
            </div>
          )}
        />

        <div className={`flex-1`} />

        <Button
          onClick={() => {
            context.showCharacterSelect()
            clearStoredStatus()
          }}
        >
          <ChatNavAction icon={<Icon which={icons.logout} />} name="Log out" />
        </Button>
      </div>

      <div className="grid grid-rows-[auto,1fr] gap-1 overflow-y-auto">
        <div className="p-2 bg-midnight-0">
          <CharacterSummary name={context.identity} />
        </div>

        <div className="bg-midnight-1">
          <RoomTabList />
        </div>
      </div>
    </nav>
  )
}
