import Chat from "./Chat"
import type { ChatProviderProps } from "./ChatContext"
import { ChatProvider } from "./ChatContext"

export default function ChatRoot(props: Omit<ChatProviderProps, "children">) {
  return (
    <ChatProvider {...props}>
      <Chat />
    </ChatProvider>
  )
}
