import DevTools from "../dev/DevTools"
import DocumentTitle from "../dom/DocumentTitle"
import NotificationToastOverlay from "../notifications/NotificationToastOverlay"
import { useChatContext } from "./ChatContext"
import ChatNav from "./ChatNav"
import ChatRoutes from "./ChatRoutes"
import SocketStatusGuard from "./SocketStatusGuard"

export default function Chat() {
  const context = useChatContext()

  return (
    <DocumentTitle title={context.identity}>
      <SocketStatusGuard>
        <div className="flex h-full gap-1">
          <div className="hidden md:block">
            <ChatNav />
          </div>
          <div className="flex-1 min-w-0 overflow-y-auto">
            <ChatRoutes />
          </div>
        </div>
        {import.meta.env.DEV && <DevTools />}
        <NotificationToastOverlay />
      </SocketStatusGuard>
    </DocumentTitle>
  )
}
