import { memo, useDeferredValue } from "react"
import ChannelView from "../channel/ChannelView"
import NotificationListScreen from "../notifications/NotificationListScreen"
import PrivateChatView from "../privateChat/PrivateChatView"
import { useRoute } from "../router"
import { useStoreValue } from "../state/store"
import StalenessState from "../ui/StalenessState"
import { useChatContext } from "./ChatContext"
import NoRoomView from "./NoRoomView"

export default memo(function ChatRoutes() {
  const context = useChatContext()
  const actualRoute = useRoute()
  const route = useDeferredValue(actualRoute)
  const joinedChannels = useStoreValue(context.channelStore.channels, Object.is)
  const privateChats = useStoreValue(
    context.privateChatStore.openChatNames,
    Object.is,
  )

  return (
    <StalenessState isStale={actualRoute !== route}>
      {(() => {
        if (
          route.name === "channel" &&
          joinedChannels[route.params.channelId]
        ) {
          return <ChannelView {...route.params} key={route.params.channelId} />
        }

        if (
          route.name === "privateChat" &&
          privateChats[route.params.partnerName]
        ) {
          return (
            <PrivateChatView {...route.params} key={route.params.partnerName} />
          )
        }

        if (route.name === "notifications") {
          return <NotificationListScreen />
        }

        return <NoRoomView />
      })()}
    </StalenessState>
  )
})
