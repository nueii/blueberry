import type { ReactNode } from "react"
import Button from "../dom/Button"
import { useStoreValue } from "../state/store"
import { solidButton } from "../ui/components"
import LoadingOverlay from "../ui/LoadingOverlay"
import { useChatContext } from "./ChatContext"

export default function SocketStatusGuard({
  children,
}: {
  children: ReactNode
}) {
  const context = useChatContext()
  const status = useStoreValue(context.socket.status)

  switch (status) {
    case "connecting":
      return <LoadingOverlay text="Connecting..." />

    case "willReconnect":
      return <LoadingOverlay text="Failed to connect, reconnecting..." />

    case "identifying":
      return <LoadingOverlay text="Identifying..." />

    case "closed":
      return (
        <ConnectionMessage message="The connection was closed by the server.">
          <Button className={solidButton} onClick={context.showLogin}>
            Return to login
          </Button>
        </ConnectionMessage>
      )
  }

  if (status !== "online") {
    return <></>
  }

  return <>{children}</>
}

function ConnectionMessage(props: { message: string; children?: ReactNode }) {
  return (
    <>
      <p>{props.message}</p>
      {props.children}
    </>
  )
}
