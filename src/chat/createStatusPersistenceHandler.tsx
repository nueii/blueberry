import { z } from "zod"
import type { CharacterStatus } from "../character/types"
import { characterStatuses } from "../character/types"
import { decodeHtml } from "../dom/decodeHtml"
import type { ChatSocket } from "../socket/ChatSocket"
import { createCommandHandler } from "../socket/helpers"
import { jsonStorageHelpers } from "../storage/jsonStorage"

const storedStatus = jsonStorageHelpers({
  path: "status.json",
  fallback: undefined,
  schema: z
    .object({
      identity: z.string(),
      status: z.custom<CharacterStatus>(
        (value) => characterStatuses.includes(value as CharacterStatus),
        "Invalid status",
      ),
      statusmsg: z.string(),
    })
    .optional(),
})

export default function createStatusPersistenceHandler(
  identity: string,
  socket: ChatSocket,
) {
  return createCommandHandler({
    STA({ character, status, statusmsg }) {
      if (character === identity) {
        storedStatus
          .set({ identity, status, statusmsg: decodeHtml(statusmsg) })
          .catch((error) => {
            console.warn("Failed to save status", error)
          })
      }
    },

    async IDN(params) {
      const result = await storedStatus.get()

      // it'd be weird hopping onto one character,
      // then going to another character and their status gets restored
      // so clear the status when going to a new character
      if (result?.identity !== params.character) {
        clearStoredStatus()
        return
      }

      socket.send({
        type: "STA",
        params: result,
      })
    },
  })
}

export function clearStoredStatus() {
  void storedStatus.delete()
}
