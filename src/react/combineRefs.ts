import type { LegacyRef, MutableRefObject } from "react"

export default function combineRefs<T>(
  ...refs: Array<LegacyRef<T> | null | undefined>
) {
  return (instance: T | null) => {
    for (const ref of refs) {
      if (typeof ref === "function") {
        ref(instance)
      } else if (typeof ref === "object" && ref !== null) {
        ;(ref as MutableRefObject<T | null>).current = instance
      }
    }
  }
}
