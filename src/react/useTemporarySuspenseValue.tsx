import { useEffect } from "react"
import { clear, suspend } from "suspend-react"
import type { JsonValue } from "type-fest"

/**
 * Loads a value via suspense, then clears the key on unmount
 * so the value can be reloaded on the next mount
 *
 * @param fn The function to load the value
 * @param keys The keys with which to store the value.
 * The first key should be globally unique and serializable.
 * At least one value is required
 */
export function useTemporarySuspenseValue<T>(
  fn: () => Promise<T>,
  keys: [JsonValue, ...JsonValue[]],
) {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => () => clear(keys), keys)
  return suspend(fn, keys)
}
