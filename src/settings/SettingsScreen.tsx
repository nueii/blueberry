import type { ReactNode } from "react"
import { Suspense } from "react"
import { useSystemNotificationsPreference } from "../notifications/useSystemNotificationsPreference"
import usePromiseState from "../state/usePromiseState"
import ExportDataButton from "../storage/ExportDataButton"
import ImportDataButton from "../storage/ImportDataButton"
import Checkbox from "../ui/Checkbox"
import { solidButton } from "../ui/components"
import LoadingIcon from "../ui/LoadingIcon"

export default function SettingsScreen() {
  return (
    <div className="grid gap-5">
      <Suspense fallback={<LoadingIcon />}>
        <SettingsSection title="Notifications">
          <NotificationSetting />
        </SettingsSection>

        <SettingsSection
          title="Export / Import"
          description="This creates a backup file for your logs, settings, and other data. You can
          import this file to restore your settings later."
        >
          <div className="flex gap-2">
            <ExportDataButton />
            <ImportDataButton />
          </div>
        </SettingsSection>
      </Suspense>
    </div>
  )
}

function NotificationSetting() {
  const { enabled, setEnabled } = useSystemNotificationsPreference()
  const state = usePromiseState(window.Notification.permission)

  const requestNotifications = () => {
    state.setPromise(window.Notification.requestPermission())
  }

  if (state.value === "default") {
    return (
      <button className={solidButton} onClick={requestNotifications}>
        Click to enable system notifications
      </button>
    )
  }

  if (state.value === "denied") {
    return (
      <p className="text-red-400">
        Notifications have been blocked. Check your browser settings to allow
        them again.
      </p>
    )
  }

  if (state.status === "rejected") {
    return (
      <p className="text-red-400">
        Failed to fetch notification permissions: {state.error.message}
      </p>
    )
  }

  if (state.value === "granted") {
    return (
      <Checkbox
        label="Show desktop notifications when you receive a message"
        checked={enabled}
        onCheckedChange={setEnabled}
      />
    )
  }

  return <LoadingIcon />
}

function SettingsSection(props: {
  title: ReactNode
  description?: ReactNode
  children: ReactNode
}) {
  return (
    <div>
      <h3 className="mb-1 text-xl font-light font-condensed">{props.title}</h3>
      {props.description ? (
        <aside className="mb-3 text-sm">{props.description}</aside>
      ) : undefined}
      {props.children}
    </div>
  )
}
