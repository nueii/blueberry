import App from "./app/App"
import AppErrorBoundary from "./app/AppErrorBoundary"
import DocumentTitle from "./dom/DocumentTitle"
import { RouteProvider } from "./router"
import WindowFrame from "./ui/WindowFrame"

export default function Root() {
  return (
    <WindowFrame>
      <AppErrorBoundary>
        <RouteProvider>
          <DocumentTitle title={APP_NAME}>
            <App />
          </DocumentTitle>
        </RouteProvider>
      </AppErrorBoundary>
    </WindowFrame>
  )
}
