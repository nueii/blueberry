import { createRequire } from "node:module"

const require = createRequire(import.meta.url)

// eslint-disable-next-line import/no-unused-modules
export function setup() {
  require("fake-indexeddb/auto")
}
