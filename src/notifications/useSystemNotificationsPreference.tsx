import { useCallback } from "react"
import { createStore, useStoreValue } from "../state/store"
import { defaultFilesystem } from "../storage/filesystem"

const file = defaultFilesystem.file("system-notifications.txt")
const store = createStore((await file.read()) === "true")

store.listen((value) => {
  void file.write(value ? "true" : "false")
})

export function useSystemNotificationsPreference() {
  const enabled = useStoreValue(store)
  const setEnabled = useCallback((enabled: boolean) => store.set(enabled), [])
  return { enabled, setEnabled }
}
