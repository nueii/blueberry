import { decodeHtml } from "../dom/decodeHtml"
import { getAvatarUrl } from "../flist/helpers"
import type { Route } from "../router"
import { routes } from "../router"
import { createCommandHandler } from "../socket/helpers"

export function createSystemNotificationsHandler(
  route: Route,
  enabled: boolean,
) {
  return createCommandHandler({
    PRI({ character, message }) {
      if (!enabled) return

      const isPrivateChatRoute =
        route.name === "privateChat" && route.params.partnerName === character

      if (isPrivateChatRoute && document.hasFocus()) return

      const note = new window.Notification(`New message from ${character}`, {
        body: decodeHtml(message),
        icon: getAvatarUrl(character),
      })

      window.desktopApi?.browserWindow.flashWindow()

      note.addEventListener("click", () => {
        routes.privateChat({ partnerName: character }).push()
        window.focus()
        window.desktopApi?.browserWindow.focusWindow()
      })
    },
  })
}
