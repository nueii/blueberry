import type { ReactNode } from "react"
import ExternalLink from "../dom/ExternalLink"
import { getProfileUrl } from "../flist/helpers"
import Popover from "../ui/Popover"
import CharacterMenu from "./CharacterMenu"

type Props = {
  name: string
  children: ReactNode
}

export default function CharacterMenuTarget({ name, children }: Props) {
  return (
    <Popover
      trigger={({ onClick }) => (
        <ExternalLink
          href={getProfileUrl(name)}
          onContextMenu={(event) => {
            event.preventDefault()
            onClick(event)
          }}
        >
          {children}
        </ExternalLink>
      )}
      content={() => (
        <div className="w-60 bg-midnight-1">
          <CharacterMenu name={name} />
        </div>
      )}
    />
  )
}
