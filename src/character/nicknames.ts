import { useCallback } from "react"
import { z } from "zod"
import { createDictStore } from "../state/dict-store"
import { useStoreValue } from "../state/store"
import { defaultFilesystem } from "../storage/filesystem"
import { jsonStorageHelpers } from "../storage/jsonStorage"

const storedNicknamesSchema = z.record(z.string())

const nicknameStorage = jsonStorageHelpers<Record<string, string | undefined>>({
  path: "nicknames.json",
  schema: storedNicknamesSchema,
  fallback: {},
})

const nicknameStore = createDictStore<string | undefined>(() => undefined)

void nicknameStorage.get().then(nicknameStore.set)
nicknameStore.listen(nicknameStorage.set)

defaultFilesystem.onImport.listen(() => {
  void nicknameStorage.get().then(nicknameStore.set)
})

export function useNicknames() {
  return useStoreValue(nicknameStore)
}

export function useNickname(characterName: string): string | undefined {
  return useStoreValue(
    nicknameStore.select((nicknames) => nicknames[characterName]),
  )
}

export function useSetNickname(characterName: string) {
  return useCallback(
    (newNickname: string) => {
      nicknameStore.setItem(characterName, newNickname)
    },
    [characterName],
  )
}
