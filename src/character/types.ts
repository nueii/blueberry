import type * as fchat from "fchat"

export const characterStatuses = [
  "offline",
  "online",
  "away",
  "idle",
  "looking",
  "busy",
  "dnd",
  "crown",
] as const
export type CharacterStatus = typeof characterStatuses[number]
export type CharacterGender = fchat.Character.Gender

export type Friendship = {
  us: string
  them: string
}

export type Character = {
  readonly name: string
  readonly gender: CharacterGender
  readonly status: CharacterStatus
  readonly statusMessage: string
}
