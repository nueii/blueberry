import sharp from "sharp"

const sizes = [16, 32, 48, 64, 128, 256]

Promise.all(
  sizes.map((size) =>
    sharp("assets/icon-spaced.png")
      .resize(size, size)
      .toFile(`assets/icons/${size}x${size}.png`),
  ),
).catch(console.error)
