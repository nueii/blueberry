/* eslint-disable unicorn/no-process-exit */
import { execa } from "execa"
import fs from "node:fs/promises"
import { dirname, join } from "node:path"
import { fileURLToPath } from "node:url"
import { oraPromise } from "ora"
import prompts from "prompts"
import semver from "semver"
import SimpleGit from "simple-git"
import pkg from "../package.json"

const __dirname = dirname(fileURLToPath(import.meta.url))

const changelogPath = join(__dirname, "../CHANGELOG.md")
const packageJsonPath = join(__dirname, "node_modules/web/package.json")

process.on("SIGINT", () => {
  process.exit(0)
})

const git = SimpleGit()
const status = await git.status()

if (status.current !== "dev") {
  console.error("Switch to the dev branch before continuing")
  process.exit(1)
}

if (status.files.length > 0) {
  console.error(
    "Found unchanged files! Commit or stash them before continuing.",
  )
  process.exit(1)
}

// ci sanity check
if (!process.argv.includes("--skip-ci")) {
  console.info("Running CI checks...")
  const { exitCode } = await execa("pnpm", ["run", "ci"], {
    reject: false,
    stdio: "inherit",
  })
  if (exitCode !== 0) {
    process.exit(1)
  }
}

// get new version
const { newVersion } = (await prompts({
  type: "text",
  name: "newVersion",
  message: `New version:`,
  initial: semver.inc(pkg.version, "patch") ?? undefined,
  validate: (value: string) => {
    const version = semver.valid(value)
    if (!version) {
      return `Invalid version: ${value}`
    }
    if (semver.lte(version, pkg.version)) {
      return `Version must be greater than the current version (${pkg.version})`
    }
    return true
  },
})) as { newVersion: string }

await oraPromise(async () => {
  const log = await git.log({ from: `v${pkg.version}`, to: "HEAD" })
  const messages = [...log.all]
    .reverse()
    .map((commit) => `- ${commit.message}`)
    .join("\n")

  const changelogContent = await fs.readFile(changelogPath, "utf8")
  await fs.writeFile(
    changelogPath,
    changelogContent.replace(
      "<!--new-version-->",
      `<!--new-version-->\n\n## ${newVersion}\n\n${messages}`,
    ),
  )
}, "Updating changelog with commits...")

await oraPromise(async () => {
  await execa(process.env.EDITOR || "code", ["--wait", changelogPath], {
    stdio: "inherit",
  })
}, "Waiting for changelog updates...")

await oraPromise(async () => {
  await fs.writeFile(
    packageJsonPath,
    JSON.stringify({ ...pkg, version: newVersion }, undefined, 2),
    "utf8",
  )
}, "Updating package.json...")

await oraPromise(async () => {
  await execa("pnpm", ["run", "format"])
}, "Formatting code...")

await oraPromise(async () => {
  await git.add(["-A"])
  await git.commit(`v${newVersion}`)
  await git.tag([`v${newVersion}`])
}, "Committing and tagging...")

await oraPromise(async () => {
  await git.checkout("main")

  await git.merge(["dev"])

  await git.push("origin", "main")
  await git.push("origin", "main", ["--tags"])

  await git.checkout("dev")
}, "Merging and pushing from main...")

console.info("released! yay ✨")
